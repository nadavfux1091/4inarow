from __future__ import print_function
import os
import neat
import numpy as np
from inArow import Board
from multiprocessing import Pool
import random
import cProfile, pstats
def eval_genomes(genomes, config):
    roundCount = 0
    results = []
    for genomes_id, genome in genomes:
        genome.fitness = 0
    p = Pool()
    for idx1, val in enumerate(genomes):
        genome_id1, genome1 = val[0], val[1]
        for  idx2, val in enumerate(genomes[idx1 + 1:]):
            genome_id2, genome2 = val[0], val[1]
            result = p.apply_async(match, args=(idx1, idx2 + idx1, neat.nn.FeedForwardNetwork.create(genome1, config),
                                                neat.nn.FeedForwardNetwork.create(genome2, config)))  # , callback=self.addFitness)
            results.append(result)
        roundCount += 1
        print(roundCount)
    p.close()
    p.join()
    for result in results:
        result = result.get()
        genomes[result[0]][1].fitness += result[2]
        genomes[result[1]][1].fitness += result[3]


def match(id1,id2,player1net, player2net):
    addToOne = 0;
    addToTwo = 0;
    board = Board()
    res = random.randint(0,1)
    if (res == 1):
        player1= player1net
        player2 = player2net
    else:
        player1 = player2net
        player2 = player1net
    x,y = 0,0
    while(board.result(x,y) == "*"):
        if board.turn == True:
            outputs = player1.activate(board.cells.flatten())
            play = np.argmax(outputs)
            while (not board.playable(play)):
                outputs[play] = -1
                play = np.argmax(outputs)
        else:
            outputs = player2.activate(board.cells.flatten())
            play = np.argmax(outputs)
            while (not board.playable(play)):
                outputs[play] = -1
                play = np.argmax(outputs)
        x,y = board.push(play)
    if(board.result(x,y) == -1):
        if (res == 1):
            addToTwo +=1
        else:
            addToOne+=1
    elif(board.result(x,y) == 1):
        if (res == 1):
            addToOne += 1
        else:
            addToTwo += 1
    else:
        addToOne += 0.5
        addToTwo += 0.5
    return id1,id2,addToOne, addToTwo


def run(config_file):
    # Load configuration.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_file)

    # Create the population, which is the top-level object for a NEAT run.
    p = neat.Population(config)

    # Add a stdout reporter to show progress in the terminal.
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(5))

    # Run for up to 300 generations.
    winner = p.run(eval_genomes, 70)

    # Display the winning genome.
    print('\nBest genome:\n{!s}'.format(winner))

    # Show output of the most fit genome against training data.
    print('\nOutput:')
    winner_net = neat.nn.FeedForwardNetwork.create(winner, config)

    print(winner.fitness)


    print(winner)

    p = neat.Checkpointer.restore_checkpoint('neat-checkpoint-99')
    p.run(eval_genomes, 70)


if __name__ == '__main__':
    # Determine path to configuration file. This path manipulation is
    # here so that the script will run successfully regardless of the
    # current working directory.
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config')
    run(config_path)
