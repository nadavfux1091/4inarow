import numpy as np
rows = 6
columns = 7
inArow = 4
class Board:
    def __init__(self):
        self.cells = np.zeros([rows, columns])
        self.turn = True
        self.moveCounter = 0

    def push(self, index):
        y = 0
        while self.cells[y,index] != 0:
            y+=1
        if(y>rows):
            return False
        else:
            self.cells[y,index] = int(self.turn) * 2 - 1
            self.turn = not self.turn
            self.moveCounter += 1
            return index,y

    def playable(self, index):
        return (self.cells[rows - 1,index] == 0)

    def print(self):
        mydict= {0:" ", 1:"x", -1:"o"}
        for row in range(0,rows):
            print(mydict[self.cells[rows-row - 1][0]] + "|" + mydict[self.cells[rows-row - 1][1]] + "|" +mydict[self.cells[rows-row - 1][2]] + "|" +mydict[self.cells[rows-row-1][3]] + "|" +mydict[self.cells[rows-row-1][4]] + "|" +mydict[self.cells[rows-row-1][5]] + "|" +mydict[self.cells[rows-row- 1][6]])
            print("-------------------")
        print()
        print()

    def result(self, x=0,y=0):
        if(self.cells[y,x] == 0):
            return"*"
        if(self.moveCounter >= 42):
            return 0.5
        offset = 1
        count = 1
        rightFlag = True
        leftFlag = True
        while(count < inArow and offset <= inArow  and (rightFlag or leftFlag)):
            if rightFlag:
                if x+offset < columns:
                    if (self.cells[y,x+offset] == self.cells[y,x]):
                        count+=1
                    else:
                        rightFlag = False
                else:
                    rightFlag = False
            if leftFlag:
                if (x-offset >= 0):
                    if (self.cells[y, x - offset] == self.cells[y, x]):
                        count += 1
                    else:
                        leftFlag = False
                else:
                    leftFlag = False
            offset+=1
        if(count >= inArow):
            return self.cells[y, x]


        offset = 1
        count = 1
        leftFlag = True
        while (count < inArow  and offset <= inArow  and leftFlag):
            if (y - offset >= 0):
                if (self.cells[y - offset, x] == self.cells[y, x]):
                    if leftFlag:
                        count += 1
                else:
                    leftFlag = False
            else:
                leftFlag = False
            offset+=1
        if (count >= inArow):
            return self.cells[y, x]

        offset = 1
        count = 1
        rightFlag = True
        leftFlag = True
        while (count < inArow  and offset <= inArow and (rightFlag or leftFlag)):
            if rightFlag:
                if y + offset < rows and x+ offset < columns:
                    if (self.cells[y + offset, x + offset] == self.cells[y, x]):
                        count += 1
                    else:
                        rightFlag = False
                else:
                    rightFlag = False
            if leftFlag:
                if (y - offset >= 0 and x- offset >= 0):
                    if (self.cells[y - offset, x - offset] == self.cells[y, x]):
                        count += 1
                    else:
                        leftFlag = False
                else:
                    leftFlag = False
            offset+=1
        if (count >= inArow):
            return self.cells[y, x]

        offset = 1
        count = 1
        rightFlag = True
        leftFlag = True
        while (count < inArow  and offset <= inArow  and (rightFlag or leftFlag)):
            if rightFlag:
                if y + offset < rows and x - offset >= 0:
                    if (self.cells[y + offset, x - offset] == self.cells[y, x]):
                        count += 1
                    else:
                        rightFlag = False
                else:
                    rightFlag = False
            if leftFlag:
                if (y - offset >= 0 and x + offset < columns):
                    if (self.cells[y - offset, x + offset] == self.cells[y, x]):
                        count += 1
                    else:
                        leftFlag = False
                else:
                    leftFlag = False
            offset+=1
        if (count >= inArow):
            return self.cells[y, x]

        return "*"

