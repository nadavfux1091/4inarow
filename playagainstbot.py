from __future__ import print_function
import os
import neat
import numpy as np
from inArow import Board
from multiprocessing import Pool

def eval_genomes(genomes, config):
    roundCount = 0
    results = []
    for genomes_id, genome in genomes:
        genome.fitness = 0
    p = Pool()
    for idx1, val in enumerate(genomes):
        genome_id1, genome1 = val[0], val[1]
        for  idx2, val in enumerate(genomes[idx1 + 1:]):
            genome_id2, genome2 = val[0], val[1]
            result = p.apply_async(match, args=(idx1, idx2 + idx1, genome1, neat.nn.FeedForwardNetwork.create(genome1, config),
                                                neat.nn.FeedForwardNetwork.create(genome2, config), genome2))  # , callback=self.addFitness)
            results.append(result)
        roundCount += 1
        print(roundCount)
    p.close()
    p.join()
    for result in results:
        print("ended")
        result = result.get()
        genomes[result[0]][1].fitness += result[2]
        genomes[result[1]][1].fitness += result[3]

def match(id1,id2,player1,player1net, player2net, player2):
    addToOne = 0;
    addToTwo = 0;
    board = Board()
    x,y = 0,0
    while(board.result(x,y) == "*"):
        if board.turn == True:
            outputs = player1net.activate(board.cells.flatten())
            play = np.argmax(outputs)
            while (not board.playable(play)):
                outputs[play] = -1
                play = np.argmax(outputs)
        else:
            outputs = player2net.activate(board.cells.flatten())
            play = np.argmax(outputs)
            while (not board.playable(play)):
                outputs[play] = -1
                play = np.argmax(outputs)
        x,y = board.push(play)
    if(board.result(x,y) == -1):
        addToTwo +=1
    elif(board.result(x,y) == 1):
        addToOne += 1
    else:
        addToOne += 0.5
        addToTwo += 0.5

    board = Board()
    while(board.result(x,y) == "*"):
        if board.turn == True:
            outputs = player2net.activate(board.cells.flatten())
            play = np.argmax(outputs)
            while (not board.playable(play)):
                outputs[play] = -1
                play = np.argmax(outputs)
        else:
            outputs = player1net.activate(board.cells.flatten())
            play = np.argmax(outputs)
            while (not board.playable(play)):
                outputs[play] = -1
                play = np.argmax(outputs)
        board.push(play)
    if(board.result(x,y) == -1):
        addToOne += 1
    elif(board.result(x,y) == 1):
        addToTwo += 1
    else:
        addToOne += 0.5
        addToTwo += 0.5
    return id1,id2,addToOne, addToTwo
def run(config_file):
    # Load configuration.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_file)
    p = neat.Checkpointer.restore_checkpoint('neat-checkpoint-69')
    winner  = p.run(eval_genomes, 1)
    winner_net = neat.nn.FeedForwardNetwork.create(winner, config)
    while True:
        board = Board()
        x,y = 0,0
        while(board.result(x,y) == "*"):
            if board.turn == True:
                outputs = winner_net.activate(board.cells.flatten())
                play = np.argmax(outputs)
                while (not board.playable(play)):
                    outputs[play] = -1
                    play = np.argmax(outputs)
            else:
                play = int(input())
                while (not board.playable(play)):
                    play = int(input())
            x,y = board.push(play)
            board.print()

if __name__ == '__main__':
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config')
    run(config_path)